# PoDrodze

[![](https://img.shields.io/badge/website-po--drodze.pl-blue.svg)](https://po-drodze.pl)
[![](https://img.shields.io/badge/version-v0.0.5-blue.svg)]()
[![](https://img.shields.io/badge/node->=_8.0.0-brightgreen.svg)]()
[![](https://img.shields.io/badge/license-UNLICENSED-lightgray.svg)]()

<!-- **[Strona główna projektu](https://po-drodze.pl/)** -->

> **PoDrodze** to darmowa aplikacja internetowa wyszukująca warte zobaczenia miejsca w pobliżu wybranej przez użytkownika trasy.

Zadaniem aplikacji jest jak największa pomoc użytkownikowi w dobraniu dodatkowych miejsc, które mógłby odwiedzić planując swoją podróż.

Aplikacja bazuje na danych publicznych, w obecnej wersji jedynie na zestawie danych ["Pomniki kultury"](https://dane.gov.pl/dataset/168)

#### Monorepo

Obecnie używane jest repozytorium monolityczne, które zawiera:

+ aplikację internetową (`web/`),
+ ustandaryzowane dane  w formacie GeoJSON (`dataInsertion/`),
+ dokumentację projektu w formie PDF (`docks/`)

## Plany

W najbliższej przyszłości chcemy przede wszystkim powiększać zbiory dostępnych danych oraz udostępnić możliwość dodawania przystanków nie tylko zaproponowanych przez aplikację.

Planujemy serię udogodnień dla użytkowników, takich jak załączanie dodatkowych danych o obiektach znajdowanych przez aplikację, czy poprawy interfejsu.

Zamierzamy dodać możliwość tworzenia kont, zapisywania ulubionych bądź odwiedzonych lokalizacji oraz dzielenia się nimi.

Ostatecznie chcielibyśmy także dać użytkownikom sposobność do włączenia się w tworzenie aplikacji poprzez zamieszczanie opinii oraz informacji o poszczególnych miejscach.

W międzyczasie będziemy dążyć do wymiany API Google oraz Google Maps na rozwiązania Open Source, takie jak Open Street Maps i Leaflet oraz Open Street Maps Router.
Router chcielibyśmy móc hostować sami, a dotychczasowy brak takiej możliwości uniemożliwił nam skorzystanie z w/w rozwiązań w początkowej fazie projektu.

## Użyte tehnologie

+ [Node.js](https://nodejs.org/en/) - platforma uruchomieniowa dla JavaScriptu
+ [Google Maps API](https://cloud.google.com/maps-platform/) - dostawca map i usługi generowania trasy
(planowane jest zastąpienie rozwiązaniami Open Source)
+ [W3.CSS](https://www.w3schools.com/w3css/default.asp) - lekki framework CSS do szybkiego prototypowania
+ [Stylus](http://stylus-lang.com/) - nadzbiór języka CSS, wykorzystany do generowania arkuszy stylów
+ [Letsencrypt](https://letsencrypt.org/) - urząd certyfikacji zapewniający bezpieczne połączenie https

### Źródła danych

+ [dane.gov.pl](http://www.dropwizard.io/1.0.2/docs/) - Obszerny zbiór danych publicznych

## Autorzy

+ **Mikołaj Cankudis** - _BackEnd Developer_ - [Mikołaj Cankudis](https://gitlab.com/mcankudis)

+ **Konrad Szychowiak** - _FrontEnd Developer & Designer_ - [Konrad Szychowiak](https://gitlab.com/konrad-szychowiak);

## Uruchamianie aplikacji na własnym sprzęcie

Poniższe instrukcje objaśniają sposób ściągnięcia i przygotowania aplikacji do uruchomienia i testowania.

### Ściągnięcie aktualnej wersji repozytorium

Aktualną wersję projektu znajdzesz na stronie `https://gitlab.com/sparks-team/po-drodze`.

Możesz ją ściągnąć pobierając archiwum z kodem źródłowym (upewnij się, że pobierasz kod z gałęzi master).

Możesz też sklonować repozytorium korzystając z konsoli (wymaga posiadania programu `git`)  

```bash
# z użyciem protokołu https
git clone https://gitlab.com/sparks-team/po-drodze.git --branch master

# z użyciem protokołu git (ssh)
git clnoe git@gitlab.com:sparks-team/po-drodze.git --branch master
```

### Docker

Aplikacja może zostać uruchomiona w kontenerze za pomocą aplikacji [Docker](https://www.docker.com/)

Pliki uruchomieniowe Dockera znajdują się w katalogu `web/`

```bash
# jeśli korzystasz z konsoli możesz ściągnąć repozytorium projektu używając komend:
git clone https://gitlab.com/sparks-team/po-drodze.git

# w przeciwnym wypadku ściągnij i rozpakuj archiwum

cd po-drodze/web/ # przejdź do katalogu aplikacji
docker-compose up # Docker zrobi wszystko sam. Dodaj opcję -d aby uruchomić w tle

# jeśli serwer został poprawnie uruchomiony, aplikacja będzie dostępna pod adresem
# localhost:3003/
```

### Node.js

Jeśli jednak chcesz uruchomić aplikację bez użycia kontenera, możesz to zrobić za pomocą Node.js

#### Wymagania

+ [Node.js](https://nodejs.org/en/download/) - środowisko uruchomieniowe JavaScript (zalecana wersja 8+);

+ [npm](https://www.npmjs.com/) – manager pakietów Node, dostarczany wraz z nim.

#### Instalacja

```bash
# jeśli korzystasz z konsoli możesz ściągnąć repozytorium projektu używając komend:
git clone https://gitlab.com/sparks-team/po-drodze.git

# w przeciwnym wypadku ściągnij i rozpakuj archiwum

cd po-drodze/web/ # przejście do katalogu aplikacji
npm install # instalacja potrzebnych modułów Node'a
npm start # uruchomienie aplikacji

## jeśli serwer został poprawnie uruchomiony, aplikacja będzie dostępna pod adresem
# localhost:3003/
## w przeglądarce internetowej
```

### Jak to działa?

Dla zadanych punktów najpierw bezpośrednio za pomocą Google Maps wyświetlana jest użytkownikowi trasa. W tym czasie na nasz serwer przesyłane są punkty: startowy, końcowy i pośrednie.
Serwer również wykonuje zapytanie do Google Directions API i w otrzymanej trasie znajduje wszystkie opisy koordynatów i na ich podstawie generuje przybliżoną trasę
(łącząc koordynaty mocno zbliżone oraz dodając koordynaty pomiędzy te zbyt od siebie oddalone). Dla tej heurystycznej trasy wyszukuje punkt po punkcie obiekty w zasięgu
zbliżonym do tego podanego przez użytkownika.

## Uwagi

<!--
[![](https://img.shields.io/badge/changelog-Keep_a_Changelog_1.0.0-orange.svg)]()

[![](https://img.shields.io/badge/versioning-Semantic_Versioning_2.0.0-orange.svg)]()

[![](https://img.shields.io/badge/branching-Nvie_Model-orange.svg)]()
-->

+ [CHANGELOG](./CHANGELOG.md) jest zgodny ze specyfikacją [*Keep a Changelog*](https://keepachangelog.com/en/1.0.0/) 1.0.0

+ Wersjonowanie jest zgodne ze specyfikacją [*Semantic Versioning*](https://semver.org/) 2.0.0

+ Struktura repozytorium jest zgodna ze specyfikacją [*nvie.com*](https://nvie.com/posts/a-successful-git-branching-model/)

## Licencja

Prototyp aplikacji PoDrodze nie jest udostępniany na publicznej licencji, ponieważ nie zostały zakończone prace nad w pełni kompletną wersją produkcyjną.
