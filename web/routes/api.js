"use strict"
const express = require('express');
const fs = require('fs');
const request = require('request');
const router = express.Router();
const san = require('../config/sanitize.js');

router.post('/getObjects', (req, res) => {
  // sanitize and initialize section
  if(typeof(req.body.start)!="string") return res.sendStatus(401)
  if(typeof(req.body.stop)!="string") return res.sendStatus(401)
  let start = parseAdress(req.body.start);
  let stop = parseAdress(req.body.stop);
  let wp = req.body.wp;
  let acc = Number(req.body.acc)/100;
  if(!san.isValidString(start)) {console.log(start);return res.sendStatus(401)};
  if(!san.isValidString(stop)) {console.log(stop);return res.sendStatus(401)};
  if(!san.isValidString(wp)&&wp!="") {console.log(wp);return res.sendStatus(401)};
  if(!san.isValidNumber(acc)) {console.log(acc);return res.sendStatus(401)};
  // -- eof s&i
  if(wp&&wp!="") wp = wp.trim()
  let url = 'https://maps.googleapis.com/maps/api/directions/json?origin='
    url+=start;
    url+='&destination=';
    url+=stop;
    if(wp&&wp!="") url+='&waypoints=optimize:true'+wp;
    url+='&key=AIzaSyDC0-zo9EgaFKv9kUtkD4LYJCevcLjraqQ';
  request.get(url, (err,resp,body) => {
    if(err) {
      console.error(err);
      res.sendStatus(404);
    }
    console.log('statusCode:', resp && resp.statusCode);
    let x = JSON.parse(body);
    if(!x) return res.sendStatus(404);
    if(!x.routes[0]) return res.sendStatus(404);
    let cords = [];
    let order = x.routes[0].waypoint_order;
    for (var j = 0; j < x.routes[0].legs.length; j++) {
      for (let i = 0; i < x.routes[0].legs[j].steps.length; i++) {
        let tmp = [x.routes[0].legs[j].steps[i].end_location.lng,x.routes[0].legs[j].steps[i].end_location.lat];
        cords.push(tmp);
      }
    }
    let geoObjects = {
      "type": "FeatureCollection",
      "features": []
    }
    let r = createPoints(cords);
    let pomniki = JSON.parse(fs.readFileSync('data/pomniki.json'));
    let d = pomniki.features;
    let taken = [];
    let objects = [];
    for (var i = 0; i < d.length; i++) taken[i] = false;
    for (var i = 0; i < r.length; i++) objects = findObj(r[i], objects, d, taken, acc);
    geoObjects.features = objects;
    res.json({objects: geoObjects, order: order});
  })
})

function calcDist(a,b) {
  let x, y;
  if(a[0]>=b[0]) x = a[0]-b[0];
  else x=b[0]-a[0];
  if(a[1]>=b[1]) y = a[1]-b[1];
  else y=b[1]-a[1];
  let p = Math.sqrt((x*x)+(y*y));
  return p;
}

function createPoints(cords) {
  let res = []; let j = 1; let temp = []; let i = 0;

  while (i < cords.length-1) {
    j=i+1;
    while(j<cords.length-1 && calcDist(cords[i], cords[j])<0.01) j++;
    i=j-1;

    if(calcDist(cords[i], cords[i+1])>0.1) {
      temp = getMiddlePoints(cords[i], cords[i+1]);
      for (let i = 0; i < temp.length; i++) {
        res.push(temp[i]);
      }
    } else res.push(cords[i])
    i++;
  }
  return res;
}

function getMiddlePoints(start, stop) {
  let res = [];
  if(calcDist(start, stop)>0.1) {
    let mid = middlePoint(start, stop);
    let r1 = getMiddlePoints(start, mid);
    let r2 = getMiddlePoints(mid, stop);
    for (var i = 0; i < r1.length; i++) {
      res.push(r1[i]);
      res.push(r2[i]);
    }
    return res;
  } else {
    return [stop];
  }
}

function middlePoint(a,b) {
  let x = (a[0]+b[0])/2;
  let y = (a[1]+b[1])/2;
  return [x,y];
}

function findObj(a, objects, d, taken, acc) {
  for (var i = 0; i < d.length; i++) {
    if(!taken[i]) {
      if(calcDist(d[i].geometry.coordinates, a)<acc) {
        objects.push(d[i]);
        // console.log(d[i]);
        taken[i] = true;
      }
    }
  }
  return objects;
}

function parseAdress(a) {
  let b = "";
  for (var i = 0; i < a.length; i++) {
    switch (a[i]) {
      case 'ą':
        b+= 'a';
        break;
      case 'ć':
        b+= 'c';
        break;
      case 'ę':
        b+= 'e';
        break;
      case 'ł':
        b+= 'l';
        break;
      case 'ó':
        b+= 'o';
        break;
      case 'ś':
        b+= 's';
        break;
      case 'ż':
        b+= 'z';
        break;
      case 'ź':
        b+= 'z';
        break;
      case 'ń':
        b+= 'n';
        break;
      case 'Ą':
        b+= 'A';
        break;
      case 'Ć':
        b+= 'C';
        break;
      case 'Ę':
        b+= 'E';
        break;
      case 'Ł':
        b+= 'L';
        break;
      case 'Ó':
        b+= 'O';
        break;
      case 'Ś':
        b+= 'S';
        break;
      case 'Ź':
        b+= 'Z';
        break;
      case 'Ź':
        b+= 'Z';
        break;
      case 'Ń':
        b+= 'N';
        break;
      default:
        b+=a[i];
    }
  }
  // REGEX CZY ZAWIERA ZNAKI POZA abc... ABC... => return false
  return b;
}
module.exports = router;
