# web/client

Katalog zawiera strony html dla strony początkowej i strony obsługi trasy, odpowiednio w plikach `index.html` i `map.html`.

##  web/client/css

Podkatalog `css/` zawiera arkusze stylów formatujące strony HTML w wersji rozwiniętej i skompresowanej.

| plik HTML    | plik CSS        | plik CSS (minified) | plik LESS         |
| ------------ | --------------- | ------------------- | ----------------- |
| `index.html` | `css/index.css` | `css/index.min.css` | `less/index.less` |
| `map.html`   | `css/map.css`   | `css/map.min.css`   | `less/map.less`   |

## web/client/less

Katalog zawiera pliki źródłowe formatu **less**, będącego nadzbiorem języka css. Format less pozwala na korzystanie m.in. ze zmiennych, funkcji i zagnieżdżania, co jest pomocne w utrzymywaniu kodu. Niestety czasami dzieje się to kosztem czytelności plików `.css`.
