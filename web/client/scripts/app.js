"use strict";

let map;
let directionsService;
let directionsDisplay;
let infoWindow;
let markers = [];

let latLngs = [];
let ungoogledLL = [];
let placesCords = [];
let ungoogledPC = [];
let waypointInRoute = [];

let iconsByType = {
  'dzieło architektury i budownictwa':  'img/bud.svg',
  'krajobraz kulturowy':                'img/kult.svg',
  'pole bitwy':                         'img/btw.svg',
  'układ urbanistyczny':                'img/urb.svg',
  'budownictwo obronne':                'img/obr.svg',
  'zabytek archeologiczny':             'img/arch.svg',
  'zabytek techniki':                   'img/tech.svg',
  'zabytkowy park i ogród':             'img/park.svg',
  'zespół rezydencjonalny':             'img/rez.svg',
  'zespół sakralny i sepulkralny':      'img/sakr.svg',
}
let icons = [{url: 'img/blue_icon.png'},{url: 'img/green_icon.png'},{url: 'img/yellow_icon.png'},{url: 'img/red_icon.png'}]
let params = (new URL(document.location)).searchParams;
let start = params.get("start");
let stop = params.get("stop");
if(start&&stop) {
  $('[name="start"]').val(start);
  $('[name="stop"]').val(stop);
}
function adjustRoute(id) {
  if(waypointInRoute[id]) removeWaypoint(id);
  else addWaypoint(id);
}
function addWaypoint(id) {
  waypointInRoute[id] = true;
  latLngs.push({location: placesCords[id], stopover: true});
  ungoogledLL.push(ungoogledPC[id]);
  findObjects();
}
function removeWaypoint(id) {
  waypointInRoute[id] = false;
  $('#obj'+id).removeClass('inRoute');
  latLngs.splice(latLngs.indexOf(placesCords[id]),1);
  ungoogledLL.splice(latLngs.indexOf(placesCords[id]),1);
  findObjects();
}
let getIcon = function(r) {
  // let i = Math.round(Math.random()*4);
  return icons[0];
}

function findObjects() {
  cleanup();
  let origin = $('[name="start"]').val();
  let dest = $('[name="stop"]').val();
  let acc = $('[name="acc"]').val();
  if(!acc) {
    acc = 20;
    $('[name="acc"]').val(acc);
  }
  let waypoints = latLngs;
  let request = {
    origin: origin,
    destination: dest,
    optimizeWaypoints: true,
    waypoints: waypoints,
    travelMode: 'DRIVING'
  };
  directionsService.route(request, function(result, status) {
    if(status == "NOT_FOUND") alert('Nie znaleziono trasy')
    else if(status == 'OK') {
      directionsDisplay.setDirections(result);
      let acc = $('[name="acc"]').val();
      let S = "";
      for (var i = 0; i < ungoogledLL.length; i++) {
        S+='|'+ungoogledLL[i].toString();
      }
      $.post("/api/getObjects", {
        start:origin,
        stop: dest,
        wp: S,
        acc: acc
      },function(res, status) {
        let results = res.objects;
        let order = res.order;
        let redirectUrl = "https://www.google.com/maps/dir/"+origin+"/"
        for (var i = 0; i < order.length; i++) {
          redirectUrl+=ungoogledLL[order[i]].toString()+"/";
        }
        redirectUrl+=dest;
        $('#openInGMaps').attr("href", redirectUrl)
        if(results.features.length>0) $("#objectsList").css('display', 'block');
        for (let i = 0; i < results.features.length; i++) {
          let f = results.features[i].properties;
          let id = f._id
          let coords = results.features[i].geometry.coordinates;
          let latLng = new google.maps.LatLng(coords[1],coords[0]);
          placesCords[id] = latLng;
          ungoogledPC[id] = [coords[1],coords[0]];
          let tmpInRoute = "";
          if(typeof(waypointInRoute[id])=="undefined") waypointInRoute[id] = false;
          else if(waypointInRoute[id]==true) tmpInRoute = "inRoute";
          $("#objectsList").append(`<div id="obj${id}" onclick="moveMap(${coords[1]}, ${coords[0]})" class="place-card ${tmpInRoute}"><city>${f.miejscowosc}</city><descr>${f.nazwa}</descr></div>`);

          let marker = new google.maps.Marker({
            position: latLng,
            map: map,
            cursor: 'pointer',
            title: f.nazwa,
            clickable: true,
            icon: getIcon(),
          });

          let contentString = `<div class="popup"><img src="${iconsByType[f.rodzaj_pomnika_historii]}"/><div class="popup__header">${f.nazwa}</div>---<div class="popup__content"> <div class="w3-third w3-mobile"><i class="fas fa-map-marker"></i> ${f.miejscowosc}</div> <div class="w3-third w3-mobile"><i class="fas fa-landmark"></i> ${f.rodzaj_pomnika_historii}</div> <div class="w3-third w3-mobile w3-button" onclick="adjustRoute(${id})"><i class="fas fa-route"></i> Dodaj/usuń</div>
          </div></div>`;

          google.maps.event.addListener(marker,'click', (function(marker,content,infoWindow){
            return function() {
                infoWindow.setContent(content);
                infoWindow.open(map,marker);
            };
          })(marker,contentString,infoWindow));
          markers.push(marker);
        }
      })
    }
  });
}
function cleanup() {
  directionsDisplay.setDirections({routes: []});
  setMapOnAll(null);
  markers = [];
  let l = $("#objectsList");
  l.empty()
  l.css('display', 'none');
  placesCords = [];
  ungoogledPC = [];
}
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 53, lng: 26},
    zoom: 4
  });
  let scaledSize = new google.maps.Size(25, 40);
  for (var i = 0; i < icons.length; i++) {
    icons[i].scaledSize = scaledSize;
  }
  infoWindow = new google.maps.InfoWindow();
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  directionsDisplay.setMap(map)
}

function moveMap(x,y) {
  map.panTo(new google.maps.LatLng(x,y))
  map.setZoom(13)
}

function setMapOnAll(map) {
  for (let i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}
$('[name="findObjectsBtn"]').click(function() {
  ungoogledLL = [];
  latLngs = [];
  findObjects();
})
$(document).ready(function() {
  if(start&&stop)findObjects();
})
