'use strict';
const sanitize = {
  isValidWord: function(string) {                          // chcecks if the string
    const regex = /\W/i;                                  // contains any NON-WORD characters
    if(regex.test(string)) return false;                  // and if any is found, FALSE is returned;
    return true;
  },
  isValidString: function(string) {                        // chcecks if the string
    const regex = /[\$\{\}"']|(http|https|\/\/)/i;        // contains any of the forbidden characters
    if(regex.test(string)) return false;                  // and if any is found, returns FALSE;
    return true;
  },
  isValidBool: function(value) {
    if(typeof value!== 'boolean') return false;
    return true;
  },
  isValidNumber: function(num) {
    if(typeof(num)!="number") return false;
    return true;
  }
}

module.exports = sanitize
