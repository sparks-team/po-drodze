# web/data

W tym katalogu znajdują się dane obiektów sformatowane z danych rządowych do "naszych danych" przez algorytmy z katalogu "dataInsertion".
Dodatkowo w podkatalogu analize/ umieściliśmy pliki, które były szkieletem obecnej funkcji dobierania obiektów przez api.

**Ich role były następujące:**

+ `routeAnalizer.js`

  Było to narzędzie do "podglądania" oraz przetwarzania danych zwracanych w pierwszym etapie projektu przez open street routing machine, a obecnie przez Google Maps Directions API.

+ `attach.js`

  Program ten był podstawą funkcji znajdywania obiektów dla odpowiednio przygotowanej przez routeAnalizer.js trasy

+ `test.js`

  Przez ten plik przewijały się przede wszystkim testy  pojedynczych funkcji/algorytmów
