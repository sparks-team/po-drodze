"use strict";
const fs = require('fs');

function getMiddlePoints(start, stop) {
  let res = [];
  // console.log(start);
  if(calcDist(start, stop)>0.1) {
    let mid = middlePoint(start, stop);
    let r1 = getMiddlePoints(start, mid);
    let r2 = getMiddlePoints(mid, stop);
    // console.log(r1);
    // console.log(r2);
    for (var i = 0; i < r1.length; i++) {
      res.push(r1[i]);
      res.push(r2[i]);
    }
    return res;
  } else {
    return [stop];
  }
}

function calcDist(a,b) {
  let x, y;
  if(a[0]>=b[0]) x = a[0]-b[0];
  else x=b[0]-a[0];
  if(a[1]>=b[1]) y = a[1]-b[1];
  else y=b[1]-a[1];
  let p = Math.sqrt(x*x+y*y);
  return p;
}

function middlePoint(a,b) {
  let x = (a[0]+b[0])/2;
  let y = (a[1]+b[1])/2;
  return [x,y];
}
let a = [52.4064, 16.9252], b=[52.2297, 21.0122];
// console.log(calcDist(a,b));

let tab = (getMiddlePoints(a,b));

let Obj = function(L, M) {
  this.type="Feature";
  this.geometry={
    type: "Point",
    coordinates: [M,L]
  }
  this.properties = {
    ispire_id: 'test'
  }
}

let geoObjects = {
  "type": "FeatureCollection",
  "features": []
}

for (let i = 0; i < tab.length; i++) {
  console.log(tab[i]);
  let newObj = new Obj(tab[i][0], tab[i][1]);
  geoObjects.features.push(newObj)
}

fs.writeFile("res.json", JSON.stringify(geoObjects), function(err) {
  if(err) {
    return console.log(err);
  }
})
