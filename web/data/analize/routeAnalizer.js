"use strict"
const fs = require('fs');
const request = require('request');
// let data = fs.readFileSync('sample_route.txt');
// data = JSON.parse(data);
// console.log(data);

let Obj = function(M, L) {
  this.type="Feature";
  this.geometry={
    type: "Point",
    coordinates: [M,L]
  }
  this.properties = {
    ispire_id: 'test'
  }
}

let geoObjects = {
  "type": "FeatureCollection",
  "features": []
}

let e1=16.828616,n1=52.4199508,e2=20.938833,n2=52.203087;
// request.get('https://router.project-osrm.org/route/v1/driving/'+e1+','+n1+';'e2+','+n2+'?overview=false&alternatives=true&steps=true&hints=;', (err,res,body) => {
//   console.log('error:', err); // Print the error if one occurred
//   console.log('statusCode:', res && res.statusCode); // Print the response status code if a response was received
//   let x = JSON.parse(body);
//   // console.log(x);
//   // console.log('body:', x.routes[0].legs[0].steps);
//   let cords = [];
//   for (let i = 0; i < x.routes[0].legs[0].steps.length; i++) {
//     cords.push(x.routes[0].legs[0].steps[i].maneuver.location);
//   }
//   let points = createPoints(cords);
//   let geoObjects = [];
//   for (let i = 0; i < points.length; i++) {
//     console.log(points[i]);
//     let newObj = new Obj(points[i][0], points[i][1]);
//     geoObjects.features.push(newObj)
//   }
//   fs.writeFile("points.json", JSON.stringify(JSON.parse(geoObjects)), (err) => {
//     if(err) throw err;
//   })
// })

// let body = fs.readFileSync("route.json");
// let x = JSON.parse(body);
// // console.log(x);
// // console.log('body:', x.routes[0].legs[0].steps);
// let cords = [];
// for (let i = 0; i < x.routes[0].legs[0].steps.length; i++) {
//   cords.push(x.routes[0].legs[0].steps[i].maneuver.location);
// }
// let points = createPoints(cords);
// fs.writeFile("pointsRaw.json", JSON.stringify(points), (err) => {
//   if(err) throw err;
// })
// for (let i = 0; i < points.length; i++) {
//   let newObj = new Obj(points[i][0], points[i][1]);
//   geoObjects.features.push(newObj)
// }
// fs.writeFile("points.json", JSON.stringify(geoObjects), (err) => {
//   if(err) throw err;
// })

function createPoints(cords) {
  let res = []; let j = 1; let temp = []; let i = 0;

  while (i < cords.length-1) {
    j=i+1;
    while(j<cords.length-1 && calcDist(cords[i], cords[j])<0.01) j++;
    i=j-1;

    if(calcDist(cords[i], cords[i+1])>0.1) {
      temp = getMiddlePoints(cords[i], cords[i+1]);
      for (let i = 0; i < temp.length; i++) {
        res.push(temp[i]);
      }
    } else res.push(cords[i])
    i++;
  }
  return res;
}

function getMiddlePoints(start, stop) {
  let res = [];
  if(calcDist(start, stop)>0.1) {
    let mid = middlePoint(start, stop);
    let r1 = getMiddlePoints(start, mid);
    let r2 = getMiddlePoints(mid, stop);
    for (var i = 0; i < r1.length; i++) {
      res.push(r1[i]);
      res.push(r2[i]);
    }
    return res;
  } else {
    return [stop];
  }
}

function calcDist(a,b) {
  let x, y;
  if(a[0]>=b[0]) x = a[0]-b[0];
  else x=b[0]-a[0];
  if(a[1]>=b[1]) y = a[1]-b[1];
  else y=b[1]-a[1];
  let p = Math.sqrt((x*x)+(y*y));
  return p;
}

function middlePoint(a,b) {
  let x = (a[0]+b[0])/2;
  let y = (a[1]+b[1])/2;
  return [x,y];
}
