"use strict"
const fs = require('fs');
let r = JSON.parse(fs.readFileSync('pointsRaw.json'));
let pomniki = JSON.parse(fs.readFileSync('pomniki.json'));
let d = pomniki.features;
let p = [], res = [];
let taken = [];
let objects = [];
for (var i = 0; i < d.length; i++) {
  taken[i] = false;
}

for (var i = 0; i < r.length; i++) {
    findObj(r[i]);
}
pomniki.features = objects;
fs.writeFile("nearby.json", JSON.stringify(pomniki), (err) => {
  if(err) throw err;
})

function findObj(a) {
  for (var i = 0; i < d.length; i++) {
    if(!taken[i]) {
      if(calcDist(d[i].geometry.coordinates, a)<0.1) {
        objects.push(d[i]);
        taken[i] = true;
      }
    }
  }
}

function calcDist(a,b) {
  let x, y;
  if(a[0]>=b[0]) x = a[0]-b[0];
  else x=b[0]-a[0];
  if(a[1]>=b[1]) y = a[1]-b[1];
  else y=b[1]-a[1];
  let p = Math.sqrt((x*x)+(y*y));
  return p;
}
