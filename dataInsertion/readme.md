# dataInsertion

W tym katalogu znajdują się algorytmy przetwarzające dane z otwartych API do formatu pożądanego przez nasz serwer.

+ `pomnikiFromOds.js`

Obecnie jedynym zestawem danych wykorzystywanym przez aplikację są dane o pomnikach kultury. Ten program tworzy z pliku `.ods` dane w formacie JSON i standardzie geoJSON.

Aby go uruchomić należy posiadać Node.js w wersji 8+ oraz w tym katalogu (`po-drodze/dataInsertion/`):

+ zainstalować zależności - `npm install`

+ uruchomić program - `node pomnikiFromOds.js`

Program do działania potrzebuje zestawu danych `pomniki.ods` w podkatalogu `data/`, a jako wynik wygeneruje plik `pomniki.json`
