"use strict"
const X = require('xlsx');
const fs = require('fs');
const data = X.readFile('data/pomniki.ods');

let Obj = function(A, B, C, D, E, F, G, H, I, J, K, L, M, ID) {
  this.type="Feature";
  this.geometry={
    type: "Point",
    coordinates: [L,M]
  }
  this.properties = {
    ispire_id: A,
    nazwa: B,
    dokladnosc_polozenia: C,
    rodzaj_pomnika_historii: D,
    data_wpisu: E,
    wojewodztwo: F,
    powiat: G,
    gmina: H,
    miejscowosc: I,
    ulica: J,
    nr_adresowy: K,
    _id: ID
  }
}

let geoObjects = {
  "type": "FeatureCollection",
  "features": []
}

let i = 2;
while(data.Sheets["Sheet 1"]["A"+i]) {
  let a = data.Sheets["Sheet 1"]["A"+i]
  let b = data.Sheets["Sheet 1"]["B"+i]
  let c = data.Sheets["Sheet 1"]["C"+i]
  let d = data.Sheets["Sheet 1"]["D"+i]
  let e = data.Sheets["Sheet 1"]["E"+i]
  let f = data.Sheets["Sheet 1"]["F"+i]
  let g = data.Sheets["Sheet 1"]["G"+i]
  let h = data.Sheets["Sheet 1"]["H"+i]
  let ii = data.Sheets["Sheet 1"]["I"+i]
  let j = data.Sheets["Sheet 1"]["J"+i]
  let k = data.Sheets["Sheet 1"]["K"+i]
  let l = data.Sheets["Sheet 1"]["L"+i]
  let m = data.Sheets["Sheet 1"]["M"+i]
  if(a) a=a.v
  if(b) b=b.v
  if(c) c=c.v
  if(d) d=d.v
  if(e) e=e.v
  if(f) f=f.v
  if(g) g=g.v
  if(h) h=h.v
  if(ii) ii=ii.v
  if(j) j=j.v
  if(k) k=k.v
  if(l) l=l.v
  if(m) m=m.v
  let newObject = new Obj(a,b,c,d,e,f,g,h,ii,j,k,l,m, i-2);
  geoObjects.features.push(newObject);
  i++;
}

fs.writeFile("pomniki.json", JSON.stringify(geoObjects), function(err) {
  if(err) {
    return console.log(err);
  }
})
