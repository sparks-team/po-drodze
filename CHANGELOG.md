# Changelog

Compliant with [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) specification.

## [Unreleased]
### Changed
- Landing page layout and color palette _WIP_;

---

## [0.0.4] - 2018-10-22
### Added
- Docker deployment;
- W3.CSS framework (for prototyping; locally stored);

### Fixed
- Maintainability of `.less` files;
- Layout and input fields styling;
- Small bugs;

---

## [0.0.3] - 2018-10-21
### Changed
- Reworked stylesheets;
- Map component grid layout;

### Fixed
- Map moving (ie. LatLng exception);

---

## [0.0.2] - 2018-10-20
### Added
- Maps from Google;
- Basic map component functionalities;
- Decomposition of `.less` files into modular model;

### Changed
- Complete styling for map component;

### Removed
- Maps from Leaflet.js (due to exogenous errors with routing);

---

## [0.0.1] – 2018-10-19 [YANKED]
### Added
- Maps from Leaflet.js;
- Trivial map component functionalities;
- Landing page;
- Less file stylesheets;
- Dataset [Pomniki Historii](https://dane.gov.pl/dataset/168);
